from direct.directnotify import DirectNotifyGlobal
from direct.distributed import DistributedObject
from direct.interval.IntervalGlobal import *
from otp.speedchat import SpeedChatGlobals
from toontown.toonbase import TTLocalizer

class DistributedPolarPlaceEffectMgr(DistributedObject.DistributedObject):
    notify = DirectNotifyGlobal.directNotify.newCategory('DistributedPolarPlaceEffectMgr')

    def __init__(self, cr):
        DistributedObject.DistributedObject.__init__(self, cr)

    def announceGenerate(self):
        DistributedObject.DistributedObject.announceGenerate(self)
        DistributedPolarPlaceEffectMgr.notify.debug('announceGenerate')
        self.accept(SpeedChatGlobals.SCStaticTextMsgEvent, self.phraseSaid)
        
    def phraseSaid(self, phraseId):
        helpPhrase = 104
        if phraseId == helpPhrase:
            self.addStratusStraitEffect()

    def delete(self):
        self.ignore(SpeedChatGlobals.SCStaticTextMsgEvent)
        DistributedObject.DistributedObject.delete(self)

    def addStratusStraitEffect(self):
        DistributedPolarPlaceEffectMgr.notify.debug('addResitanceEffect')
        av = base.localAvatar
        self.sendUpdate('addStratusStraitEffect', [])
        msgTrack = Sequence(Func(av.setSystemMessage, 0, TTLocalizer.StratusStraitEffect1), Wait(2), Func(av.setSystemMessage, 0, TTLocalizer.StratusStraitEffect2), Wait(4), Func(av.setSystemMessage, 0, TTLocalizer.StratusStraitEffect3))
        msgTrack.start()
