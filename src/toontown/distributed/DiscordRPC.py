import time
from ctypes import * # Used to interact with DLL
from direct.task import Task # Used to manage the Callback timer

class DiscordRPC:
    zone2imgdesc = { # A dict of ZoneID -> An image and a description
        1000: ["toontown-reef", "In Toontown Reef"],
        1100: ["barnacle-boulevard", "On Barnacle Boulevard"],
        1200: ["seaweed-street", "On Seaweed Street"],
        1300: ["lighthouse-lane", "On Lighthouse Lane"],
        
        2000: ["toontown-central", "In Toontown Central"],
        2100: ["silly-street", "On Silly Street"],
        2200: ["loopy-lane", "On Loopy Lane"],
        2300: ["punchline-place", "On Punchline Place"],
        
        3000: ["the-brrrgh", "In The Brrrgh"],
        3100: ["walrus-way", "On Walrus Way"],
        3200: ["sleet-street", "On Sleet Street"],
        3300: ["polar-place", "On Polar Place"],
        
        4000: ["musical-melodyland", "In Musical Melodyland"],
        4100: ["alto-avenue", "On Alto Avenue"],
        4200: ["baritone-boulevard", "On Baritone Boulevard"],
        4300: ["tenor-terrace", "On Tenor Terrace"],
        
        5000: ["woody-gardens", "In Floral Gardens"],
        5100: ["blossom-boulevard", "On Blossom Boulevard"],
        5200: ["lavender-lane", "On Lavender Lane"],
        5300: ["rosey-road", "On Rosey Road"],
        
        6000: ["acorn-acres", "At Acorn Acres"],
        
        8000: ["toontown-speedway", "In Toontown Speedway"],

        9000: ["drowsy-dreamland", "In Drowsy Dreamland"],
        9100: ["lullaby_lane", "On Lullaby Lane"],
        9200: ["pajama_place", "On Pajama Place"],
        
        10000: ["bossbot-hq", "In Bossbot Headquarters"],
        10100: ["bossbot-hq", "In The CEO Lobby"],
        10500: ["bossbot-hq", "In The Front Three"],
        10600: ["bossbot-hq", "In The Middle Six"],
        10700: ["bossbot-hq", "In The Back Nine"],

        11000: ["sellbot-hq","At Sellbot Headquarters"],
        11100: ["sellbot-hq", "In The VP Lobby"],
        11200: ["sellbot-hq", "In The Sellbot Factory"],
        11500: ["sellbot-hq", "In The Sellbot Factory"],
        
        12000: ["cashbot-hq", "At Cashbot Headquarters"],
        12100: ["cashbot-hq", "In The CFO Lobby"],
        12500: ["cashbot-hq", "In The Cashbot Coin Mint"],
        12600: ["cashbot-hq", "In The Cashbot Dollar Mint"],
        12700: ["cashbot-hq", "In The Cashbot Bullion Mint"],

        13000: ["lawbot-hq", "At Lawbot Headquarters"],
        13100: ["lawbot-hq","In The CJ Lobby"],
        13200: ["lawbot-hq","In The DA Office Lobby"],
        13300: ["lawbot-hq", "In The Lawbot Office A"],
        13400: ["lawbot-hq", "In The Lawbot Office B"],
        13500: ["lawbot-hq", "In The Lawbot Office C"],
        13600: ["lawbot-hq", "In The Lawbot Office D"],

        14000: ["tutorial", "In The Toontorial"],

        16000: ["estate", "At A Toon Estate"],

        17000: ["minigames", "In The Minigames Area"],

        18000: ["party", "At A Toon Party"]
   }
    
    def __init__(self):
        self.CodeHandle = cdll.LoadLibrary("SDK.dll") # Load the RP code
        self.CodeHandle.DLLMain()
        self.UpdateTask = None
        self.details = "Loading" # The writing next to the photo
        self.image = "logo" #The main photo
        self.imageTxt = "Toonination" # Hover text for the main photo
        self.smallLogo = "globe" # Small photo in corner
        self.state = "" # Displayed underneath details - used for boarding groups
        self.smallTxt = "Loading" # null this out
        self.PartySize = 0
        self.MaxParty = 0

    def stopBoarding(self):  #Boarding groups :D
        self.PartySize = 0
        self.state = ""
        self.MaxParty = 0
        self.setData()

    def AllowBoarding(self, size):
        self.state = "In A Boarding Group"
        self.PartySize = 1
        self.MaxParty = size
        self.setData()

    def setBoarding(self, size): # Sets how many members are in a boarding group
        self.PartySize = size
        self.setData()

    def setData(self): # Manually update all vars
        self.CodeHandle.DoCallbacks()
        details = self.details
        image = self.image
        imageTxt = self.imageTxt
        smallLogo = self.smallLogo
        smallTxt = self.smallTxt
        state = self.state
        party = self.PartySize
        maxSize = self.MaxParty
        self.CodeHandle.SetData(details.encode('utf_8'), state.encode('utf_8'), smallLogo.encode('utf_8'), smallTxt.encode('utf_8'), image.encode('utf_8'), imageTxt.encode('utf_8'), maxSize, party)

    def DoCallbacks(self, task): # Recieves any messages from discord and handles them
        self.CodeHandle.DoCallbacks()
        return task.again

    def UpdateTasks(self, task):
        self.UpdateTask = True
        self.setData()
        return task.again

    def AvChoice(self): # Call in pick-a-toon
        self.image = "logo"
        self.details = "Picking-A-Toon"
        self.setData()

    def Launching(self): # Call When loading game - toontownstart
        self.image = "logo"
        self.details = "Loading"
        self.setData()

    def Making(self): # Call in make-a-toon
        self.image = "logo"
        self.details = "Making A Toon"
        self.setData()

    def StartTasks(self): # Call JUST before base.run() in toontown-start
        taskMgr.doMethodLater(10, self.UpdateTasks, 'UpdateTask')
        taskMgr.doMethodLater(0.016, self.DoCallbacks, 'RPC-Callbacks')

    def setZone(self,Zone): # Set image and text based on the zone
        if not isinstance(Zone, int):
            return
        Zone -= Zone % 100
        data = self.zone2imgdesc.get(Zone,None)
        if data:
            self.image = data[0]
            self.details = data[1]
            self.setData()
        else:
            print("Error: Zone Not Found!")

    def setDistrict(self,Name): # Set the image text the district name
        self.smallTxt = Name
