# Safe zones:
want-donalds-dock #f
want-daisys-garden #f
want-minnies-melodyland #f
want-the-burrrgh #f
want-donalds-dreamland #f
want-goofy-speedway #f
want-outdoor-zone #f
want-golf-zone #f

# Safe zone settings:
want-treasure-planners #f

# Cog headquarters:
want-cog-headquarters #f

# Trolley minigames:
want-minigames #f

# Cog buildings:
beach-ball-boulevard-building-min 14
beach-ball-boulevard-building-max 14
beach-ball-boulevard-building-chance 100.0
aloha-avenue-building-min 14
aloha-avenue-building-max 14
aloha-avenue-building-chance 100.0
pineapple-place-building-min 14
pineapple-place-building-max 14
pineapple-place-building-chance 100.0

# Core features:
want-fishing #f
want-housing #f
want-pets #f
want-parties #f

# Optional:
want-talkative-tyler #f
want-yin-yang #f
